//
//  PanakeiaBluetoothFrameworkTV.h
//  PanakeiaBluetoothFrameworkTV
//
//  Created by David Alarcon on 03/08/16.
//  Copyright © 2016 Panakeia. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for PanakeiaBluetoothFrameworkTV.
FOUNDATION_EXPORT double PanakeiaBluetoothFrameworkTVVersionNumber;

//! Project version string for PanakeiaBluetoothFrameworkTV.
FOUNDATION_EXPORT const unsigned char PanakeiaBluetoothFrameworkTVVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PanakeiaBluetoothFrameworkTV/PublicHeader.h>


