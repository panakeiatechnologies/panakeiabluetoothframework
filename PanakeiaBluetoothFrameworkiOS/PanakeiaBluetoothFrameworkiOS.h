//
//  PanakeiaBluetoothFrameworkiOS.h
//  PanakeiaBluetoothFrameworkiOS
//
//  Created by David Alarcon on 03/08/16.
//  Copyright © 2016 Panakeia. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for PanakeiaBluetoothFrameworkiOS.
FOUNDATION_EXPORT double PanakeiaBluetoothFrameworkiOSVersionNumber;

//! Project version string for PanakeiaBluetoothFrameworkiOS.
FOUNDATION_EXPORT const unsigned char PanakeiaBluetoothFrameworkiOSVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PanakeiaBluetoothFrameworkiOS/PublicHeader.h>


