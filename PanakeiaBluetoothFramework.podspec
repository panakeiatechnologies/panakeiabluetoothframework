Pod::Spec.new do |spec|
  spec.name = 'PanakeiaBluetoothFramework'
  spec.version = '0.0.1'
  spec.summary = "Bluetooth Framework to use in Panakeia Apps."
  spec.source = { 
    :git => "https://momachilles@bitbucket.org/panakeiatechnologies/panakeiabluetoothframework.git", 
    :tag => spec.version,
    :branch => 'development'
  }
  spec.license = 'BSD 2-Clause License' 
  spec.author = { "David Alarcon" => 'dalarcon@panakeiatechnologies.com' }
  spec.social_media_url = 'http://twitter.com/panakeiatech'
  spec.homepage = "https://bitbucket.org/panakeiatechnologies/panakeiabluetoothframework/overview"

  spec.ios.deployment_target = '10.0'

  spec.requires_arc = true
  spec.source_files = "**/*.{h,swift}"

  #spec.dependency "Curry", "~> 1.4.0"
end
