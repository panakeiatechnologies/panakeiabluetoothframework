//
//  PanakeiaBluetoothFrameworkWatch.h
//  PanakeiaBluetoothFrameworkWatch
//
//  Created by David Alarcon on 03/08/16.
//  Copyright © 2016 Panakeia. All rights reserved.
//

#import <WatchKit/WatchKit.h>

//! Project version number for PanakeiaBluetoothFrameworkWatch.
FOUNDATION_EXPORT double PanakeiaBluetoothFrameworkWatchVersionNumber;

//! Project version string for PanakeiaBluetoothFrameworkWatch.
FOUNDATION_EXPORT const unsigned char PanakeiaBluetoothFrameworkWatchVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PanakeiaBluetoothFrameworkWatch/PublicHeader.h>


