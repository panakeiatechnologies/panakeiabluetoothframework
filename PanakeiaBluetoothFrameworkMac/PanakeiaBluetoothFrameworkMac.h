//
//  PanakeiaBluetoothFrameworkMac.h
//  PanakeiaBluetoothFrameworkMac
//
//  Created by David Alarcon on 03/08/16.
//  Copyright © 2016 Panakeia. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for PanakeiaBluetoothFrameworkMac.
FOUNDATION_EXPORT double PanakeiaBluetoothFrameworkMacVersionNumber;

//! Project version string for PanakeiaBluetoothFrameworkMac.
FOUNDATION_EXPORT const unsigned char PanakeiaBluetoothFrameworkMacVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PanakeiaBluetoothFrameworkMac/PublicHeader.h>


